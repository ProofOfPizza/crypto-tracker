module Main where

import App
import System.IO
import System.Console.ANSI

main :: IO ()
main = do
  hSetBuffering stdin NoBuffering
  hSetBuffering stdout NoBuffering
  setTitle "Crypto Disco"
  runApp
