module App (runApp) where 
            
import            Data.Maybe(fromMaybe)
import            HttpClient
import            Control.Exception
import qualified  Data.Text as T
import            System.Console.ANSI
import            CustomTypes 
import            Db
import            Calculations
import            Display

runApp :: IO ()
runApp =  do  
  clearScreen
  setSGR [SetColor Foreground Vivid Magenta, SetBlinkSpeed SlowBlink]
  putStrLn "                             ... Firing up the discoballs! ... "
  setSGR [Reset]
  displayPortfolio 
  choice <- getChoice
  case choice of 
    'a' -> do
      getAddLine >>= addEntry
      runApp   
    'u' -> do
      getUpdateLine >>= updateEntry
      runApp  
    'd' -> do 
      getDeleteLine >>= deleteEntry
      runApp   
    _ -> quit

displayPortfolio :: IO ()
displayPortfolio = do
  startDb
  allEntries <- getAllEntries
  coinsInEurValue <- run (Just "EUR") (Just 0) (Just 0)
  saveCoinsList $ map (\x -> Token { ind = Nothing, tokenName = symbol x }) coinsInEurValue
  let subtots = subTotals allEntries coinsInEurValue
      tots = totals allEntries coinsInEurValue
  clearScreen
  mapM_ setAndPrint (formatPortfolio allEntries subtots tots)
    where 
      setAndPrint (sgr,str) = do
        setSGR [sgr]
        putStrLn str
  
getChoice :: IO Char
getChoice = do
  putStr "Would you like to (A)dd, (U)pdate or (D)elete and entry? Or just (Q)uit? ==> "
  c <- getLine
  let choice = T.toLower . T.pack $ c
  if choice `elem` ["a","u","d","q", "add", "update", "delete", "quit"] then return (T.head choice) else getChoice

getAddLine :: IO (T.Text, Double, Double)
getAddLine = do
  currency <- getCoin
  amount <- getAmount
  cost <- getCost
  return (currency, amount, cost)  

getUpdateLine :: IO (Int, T.Text, Double, Double)
getUpdateLine = do
  index <- getTransactionNumber
  currency <- getCoin
  amount <- getAmount
  cost <- getCost
  return (index, currency, amount, cost)  

getDeleteLine :: IO Int
getDeleteLine = getTransactionNumber

quit :: IO ()
quit = do
  setSGR [SetColor Foreground Vivid Magenta]
  putStrLn "\nThank you for using the bubble machine!\n"
  setSGR [Reset]

getTransactionNumber :: IO Int
getTransactionNumber = do
  putStr "Which entry number? ==> "
  number <- getLine
  numInt <- tryInt number
  case numInt of
    Right val -> return val
    Left _  -> getTransactionNumber

getCoin :: IO T.Text 
getCoin = do
  putStr "For which coin? E.g. MIOTA, ETH or BTC ==> "
  c <- getLine 
  coinsList <- getCoinsList
  let allCoins = map (fromMaybe "" . tokenName) coinsList
      coin = T.toUpper . T.pack $ c
  if coin `elem` allCoins 
  then do
    return coin 
  else do
    putStrLn "Coins must be an exact match in this list: "
    mapM_ print allCoins 
    getCoin

getAmount :: IO Double
getAmount = do
  putStr "How many tokens? If you are selling then this number is negative e.g. -21.34 ==> "
  amount <- getLine
  amountD <- tryDouble amount
  case amountD of
    Right val -> return val
    Left _ -> getAmount 

getCost :: IO Double
getCost = do
  putStr "How much money did it cost you? N.b if you are selling then this number is negative e.g. -109.50 ==> "
  cost <- getLine
  costD <- tryDouble cost
  case costD of
    Right val -> return val
    Left _ -> getCost 

tryInt :: String -> IO (Either SomeException Int)
tryInt s = try (evaluate (read s :: Int)) :: IO (Either SomeException Int)

tryDouble :: String -> IO (Either SomeException Double)
tryDouble s =  try (evaluate (read s :: Double)) :: IO (Either SomeException Double)

