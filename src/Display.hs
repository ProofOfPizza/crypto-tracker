module Display
    (formatPortfolio
    ) where

import Data.List
import CustomTypes
import System.Console.ANSI

{-
Every row is divided in 6 columns. It has 6 columns filled with text (or "") and filling
(spaces or - or whatnot). These are [[String]] intercalted with dividers such as | or +

-}

type Filler = Char  
type Delimiter = String
data FillPosition = L | M | R | N -- left, middle, right, number: left to center

numOfCols :: Int
numOfCols = 6

colSize ::Int
colSize = 17

formattedLine :: [FillPosition] -> Filler -> Delimiter -> [String] -> String
formattedLine [] f d [] = formattedLine (replicate numOfCols L) f d (replicate numOfCols "")
formattedLine fps f d xs = (intercalate d . map formatCol)  (zip xs fps) ++ d
  where
    formatCol (x,R) = replicate (colSize - length x-1) f ++ x ++ " "
    formatCol (x,M) = let lp = (colSize -  length x) `div` 2 
                          rp = colSize - length x - lp 
                      in (replicate lp f ++  x ++ replicate rp f)
    formatCol (x,L) = " " ++ x ++ replicate (colSize - length x -1) f
    formatCol (x,N) = replicate (colSize `div` 2 - length x) f ++ x ++ replicate (colSize - (colSize `div` 2)) f

h1 :: Int -> [String]
h1 n = [formattedLine (replicate n M) '-' "+" (repeat "")] 

h2 :: [String] 
h2 =  [formattedLine [M,M,M,M] ' ' "|" ["NO", "COIN", "AMOUNT", "INVESTED"]]

h3 :: [String] 
h3 =  [formattedLine [M,M,M,M,M,M] ' ' "|" ["", "COIN", "TOT AMOUNT", "TOT INVESTED","ACTUAL VALUE", "ROI %"]]

formatPortfolio :: (Printable a, Printable b) => [a] -> [a] -> b -> [(SGR ,String)]
formatPortfolio ws subs tots = 
  zip (repeat Reset) ["\n\n"]
  ++ zip [SetConsoleIntensity BoldIntensity] h2
  ++ zip [SetConsoleIntensity BoldIntensity] (h1 4)
  ++ zip (repeat Reset) (map (formattedLine [N, L, R, R] ' ' "|"  . toFormat) ws)
  ++ zip (repeat Reset) ["\n"]
  ++ zip [SetConsoleIntensity BoldIntensity] h3 
  ++ zip [SetConsoleIntensity BoldIntensity] (h1 6) 
  ++ zip (repeat Reset) (map (formattedLine [L, L, R, R, R, R] ' ' "|" . (++) [""] .  toFormat) subs)
  ++ zip [SetConsoleIntensity BoldIntensity] (h1 6)
  ++ zip [SetConsoleIntensity BoldIntensity] 
         [(formattedLine [L, L, M, R, R, R] ' ' "|" . (++) ["", "","TOTALS"] . toFormat) tots] 
  ++ zip [SetConsoleIntensity BoldIntensity] [formattedLine [M, M, M, M, M, M] '_' "|" ["","","","","", ""]]
  ++ zip (repeat Reset) ["\n\n"]  
