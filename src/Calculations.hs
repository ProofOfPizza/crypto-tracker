module Calculations
    ( subTotals
    , totals
    , currentValue
    , gains
    ) where

import           Data.Either(fromRight)
import           Data.Maybe(catMaybes)
import qualified Data.Text as T
import qualified Data.Text.Read as R
import           Data.List
import           CustomTypes

totals :: [WalletEntry] -> [Coin] -> Totals
totals ws cs = foldr sumWalletEntries tStart (subTotals ws cs)
  where
      tStart = Totals {totCost = Just 0.00, totValue = Just 0.00, totGains = Just 0.00}
      sumWalletEntries w t = Totals (sumCosts w t) (sumValues w t) (sumGains w t)
      sumCosts w t =  roundTo2 <$> ((+) <$> cost w <*> totCost t)
      sumValues w t = roundTo2 <$> ((+) <$> actualValue w <*> totValue t)
      sumGains w t = gains (sumCosts w t)  (sumValues w t)

subTotals :: [WalletEntry] -> [Coin] -> [WalletEntry]
subTotals ws cs =
  let getSubTotals x = profitPerLine
        (WalletEntry {index = Nothing, currency = x, amount = getAmounts x
        , cost = getCosts x, actualValue = Nothing, percentageGains = Nothing
        })  (thisCoin x cs)
      getAmounts curr = Just $ (roundTo2 . sum . catMaybes) $ foldr 
        (\(WalletEntry _ c a _ _ _) acc -> if c == curr then a : acc else acc ) [] ws
      getCosts curr = Just $ (roundTo2 . sum . catMaybes) $ foldr 
        (\(WalletEntry _ c _ co _ _) acc -> if c == curr then co : acc else acc ) [] ws
      thisCoin x = foldr (\c acc  -> if (symbol c) == x  then Just c else acc) Nothing
      uniqueCurrencies = (nub . map currency) ws
  in map getSubTotals uniqueCurrencies

profitPerLine :: WalletEntry -> Maybe Coin -> WalletEntry
profitPerLine w Nothing = w
profitPerLine w (Just c) = WalletEntry
  { index = index w
  , currency = currency w
  , amount = amount w
  , cost = cost w
  , actualValue = currentValue w c
  , percentageGains = gains (cost w) (currentValue w c)
  }
  
gains :: Maybe Double -> Maybe Double -> Maybe Double
gains invested actualValue' = (\i a -> roundTo2 (a / i * 100 - 100)) <$> invested <*> actualValue'

currentValue :: WalletEntry -> Coin -> Maybe Double
currentValue w c = roundTo2 <$> ((*) <$> (amount w) <*> (getActualPrice c))

--------- Helpers ------

roundTo2 :: Double -> Double
roundTo2 x = (fromInteger $ round $ x * (10^2)) / (10.0^^2)

text2Double :: T.Text -> Double
text2Double t = fst . fromRight (0.00, "") $ R.rational t

getActualPrice :: Coin -> Maybe Double
getActualPrice coin
  | schrinkList coin == [] = Nothing
  | otherwise = Just (text2Double . head $ schrinkList coin)
    where
      schrinkList c = catMaybes
        [ price_aud c, price_brl c, price_cad c, price_chf c, price_clp c, price_cny c
        , price_czk c, price_dkk c, price_eur c, price_gbp c, price_hkd c, price_huf c
        , price_idr c, price_ils c, price_irn c, price_jpy c, price_krw c, price_mxr c
        , price_myr c, price_nok c, price_nzd c, price_php c, price_pkr c, price_pln c
        , price_rub c, price_sek c, price_sgd c, price_thb c, price_try c, price_twd c
        , price_zar c ]
