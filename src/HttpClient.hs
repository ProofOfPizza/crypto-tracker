module HttpClient
    (run
    ) where

import qualified  Data.Text as T
import            Data.Proxy
import            Servant.API
import            Servant.Client
import            Network.HTTP.Client (newManager)
import            Network.HTTP.Client.TLS(tlsManagerSettings)

import            CustomTypes

type API = "ticker" :> QueryParam "convert" T.Text :> QueryParam "start" Int:> QueryParam "limit" Int :> Get '[JSON] [Coin]

ticker :: Maybe T.Text -> Maybe Int -> Maybe Int -> ClientM [Coin]
ticker = client api

api :: Proxy API
api = Proxy

url :: BaseUrl
url = BaseUrl Https "api.coinmarketcap.com" 443 "v1"

queries :: Maybe T.Text -> Maybe Int -> Maybe Int -> ClientM [Coin]
queries currency start quantity = do
  allCoins <- ticker (T.toUpper <$> currency) start quantity
  return (allCoins)

run :: Maybe T.Text -> Maybe Int -> Maybe Int -> IO [Coin]
run currency start quantity = do
  manager' <- newManager tlsManagerSettings
  res <- runClientM (queries currency start quantity) (mkClientEnv manager' (url))
  case res of
    Left err -> do
      putStrLn $ "Error: " ++ show err
      return []
    Right (allCoins) -> do
      return allCoins
