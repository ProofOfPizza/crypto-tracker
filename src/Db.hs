module Db
    ( startDb
    , addEntry
    , updateEntry
    , deleteEntry
    , getAllEntries
    , getCoinsList
    , saveCoinsList
    )
    where

import qualified Data.Text as T
import           Database.SQLite.Simple
import           CustomTypes

startDb :: IO ()
startDb = do
  conn <- open "crypto-tracker.db"
  execute_ conn "CREATE TABLE IF NOT EXISTS entries (id_ integer primary key, currency text not null, amount double not null, cost double not null, actualValue double, percentageGains double)"
  execute_ conn "DROP TABLE IF EXISTS coins"
  execute_ conn "CREATE TABLE coins (id_ integer primary key, coin text not null)"

addEntry :: (T.Text, Double, Double) -> IO ()
addEntry (cu, am, co) = do
  conn <- open "crypto-tracker.db"
  execute conn "INSERT INTO entries (currency, amount, cost, actualValue, percentageGains) VALUES (?,?,?,?,?)" (WalletEntry Nothing (Just (T.toUpper cu)) (Just am) (Just co) Nothing Nothing)
  close conn

updateEntry :: (Int, T.Text, Double, Double) -> IO ()
updateEntry (i, cu, am, co) = do
  conn <- open "crypto-tracker.db"
  executeNamed conn "UPDATE entries SET currency = :c, amount = :a, cost = :co where id_ = :id_"
    [":c" := cu, ":a" := am, ":co" :=co, ":id_" := i]
  close conn

deleteEntry :: Int -> IO ()
deleteEntry i = do
  conn <- open "crypto-tracker.db"
  execute conn "DELETE FROM entries WHERE id_ = (?)" (Only i)
  close conn

getAllEntries :: IO [WalletEntry]
getAllEntries = do
  conn <- open "crypto-tracker.db"
  allEntries <- query_ conn "SELECT * FROM entries" :: IO [WalletEntry]
  close conn
  return allEntries

getCoinsList :: IO [Token]
getCoinsList = do 
  conn <- open "crypto-tracker.db"
  coinsList <- query_ conn "SELECT * FROM coins" :: IO [Token]
  close conn
  return coinsList

saveCoinsList :: [Token] -> IO ()
saveCoinsList  list = do
  conn <- open "crypto-tracker.db"
  let save c = execute conn "INSERT INTO coins (coin) VALUES (?)" (Only (tokenName c)) 
  mapM_ save list 
  close conn
