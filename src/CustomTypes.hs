module CustomTypes
    ( WalletEntry(..)
    , Totals(..)
    , Coin(..)
    , Token(..)
    , Printable(toFormat)
    ) where

import qualified Data.Text as T
import           Database.SQLite.Simple.FromRow
import           Database.SQLite.Simple
import           Data.Aeson
import           GHC.Generics
import           Text.Printf

class Printable a
  where
    toFormat :: a -> [String]

data WalletEntry = WalletEntry
  { index :: Maybe Int
  , currency :: Maybe T.Text
  , amount :: Maybe Double
  , cost :: Maybe Double
  , actualValue :: Maybe Double
  , percentageGains :: Maybe Double
  } deriving Show

data Totals = Totals
  { totCost :: Maybe Double
  , totValue :: Maybe Double
  , totGains :: Maybe Double
  } deriving Show

data Token = Token {ind :: Maybe Int, tokenName :: Maybe T.Text } deriving Show

data Coin = Coin
  { id :: Maybe T.Text
  , name :: Maybe T.Text
  , symbol :: Maybe T.Text
  , rank :: Maybe T.Text
  , price_usd :: Maybe T.Text
  , price_btc :: Maybe T.Text
  , percent_change_1h :: Maybe T.Text
  , percent_change_24h :: Maybe T.Text
  , percent_change_7d :: Maybe T.Text
  , price_aud :: Maybe T.Text
  , price_brl :: Maybe T.Text
  , price_cad :: Maybe T.Text
  , price_chf :: Maybe T.Text
  , price_clp :: Maybe T.Text
  , price_cny :: Maybe T.Text
  , price_czk :: Maybe T.Text
  , price_dkk :: Maybe T.Text
  , price_eur :: Maybe T.Text
  , price_gbp :: Maybe T.Text
  , price_hkd :: Maybe T.Text
  , price_huf :: Maybe T.Text
  , price_idr :: Maybe T.Text
  , price_ils :: Maybe T.Text
  , price_irn :: Maybe T.Text
  , price_jpy :: Maybe T.Text
  , price_krw :: Maybe T.Text
  , price_mxr :: Maybe T.Text
  , price_myr :: Maybe T.Text
  , price_nok :: Maybe T.Text
  , price_nzd :: Maybe T.Text
  , price_php :: Maybe T.Text
  , price_pkr :: Maybe T.Text
  , price_pln :: Maybe T.Text
  , price_rub :: Maybe T.Text
  , price_sek :: Maybe T.Text
  , price_sgd :: Maybe T.Text
  , price_thb :: Maybe T.Text
  , price_try :: Maybe T.Text
  , price_twd :: Maybe T.Text
  , price_zar :: Maybe T.Text
  } deriving (Show, Generic)

instance ToRow WalletEntry where
  toRow (WalletEntry id_ currency amount cost actualValue percentageGains)
    | id_ == Nothing = toRow (currency, amount, cost, actualValue, percentageGains)
    | otherwise = toRow (id_, currency, amount, cost, actualValue, percentageGains)

instance ToRow Token where
  toRow (Token ind tokenName)  = toRow (ind, tokenName) 

instance FromRow Token where
  fromRow = Token <$> field <*> field
  
instance FromRow WalletEntry where
  fromRow = WalletEntry <$> field <*> field <*> field <*> field <*> field <*> field

instance Printable WalletEntry where
  toFormat (WalletEntry (Just i) (Just cu) (Just am) (Just co) Nothing Nothing) = 
    [show i, T.unpack cu, d 2 am, d 2 co]
  toFormat (WalletEntry Nothing (Just cu) (Just am) (Just co) (Just av) (Just g)) = 
    [T.unpack cu, d 2 am, d 2 co, d 2 av, d 3 g++ " %"]
  toFormat (WalletEntry (Just i) (Just cu) (Just am) (Just co) (Just av) (Just g)) = 
    [show i, T.unpack cu, d 2 am, d 2 co, d 2 av, d 3 g ++ " %"]
  toFormat _ = []

instance Printable Totals 
  where
    toFormat (Totals (Just c) (Just v) (Just g)) = [d 2 c, d 2 v, d 3 g ++ " %"]  
    toFormat _ = []

instance FromJSON Coin

----- helpers: ---

d  :: Int -> Double -> String
d n x = printf ("%." ++ show n ++ "f") x :: String
