# Crypto-tracker

## This is a simple commandline tool to track your crypto portfolio

**Install:**
git clone https://gitlab.com/ProofOfPizza/crypto-tracker.git

hpack

stack build


**How to use** 

It is simply a question of adding the lines as accurately as possible. If you only care about the totals then just add your invested money, and your cryptos. If you would like the subtotals to make sense you will have to estimate as good as you can the fiat values of all transactions. Not very elegant maybe, but it works.

**Goal**
I like commandline tools, and i missed this one.
Apart from that it is a first Haskell project, so it's all about learning.
If you have any suggestions let me know! Find me on reddit /u/NiPinga , or spacing out on irc freenode #Haskell as ProofOfPizza

_Enjoy the groove!_


