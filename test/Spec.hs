import Test.Hspec
import Calculations
main :: IO ()
main = hspec $ do
  describe "gains" $ do
    it "should calculate the percentage gains" $ do
      gains (Just  500.00) (Just 600.00) `shouldBe` Just 20.00
      gains (Just 600.00) (Just 200.00) `shouldBe` Just (-66.67)
      gains (Just 500.00) (Just 430.50) `shouldBe` Just (-13.90)

